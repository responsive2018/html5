# Curso HTML 5

Para este curso utilizaremos como editor principal Sublime Text 3, con los siguientes paquetes instalados:

* Emmet


### Temario

* Que es HTML 5 y su estructura principal
* Elementos
  * body
  * header
  * meta
  * title
  * favicon
    * con .ico
    * con imagen PNG
  * tablas
  * Listas
  * inputs
  * images
    * rutas absolutas
    * rutas relativas
  * Font Awesome
  * Header
  * Footer
  * Content
  * Sonido
  * Video
* Uso de Emmet para trabajar eficientemente
